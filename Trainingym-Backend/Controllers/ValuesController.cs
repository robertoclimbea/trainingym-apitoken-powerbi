﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors; 


namespace Trainingym_Backend.Controllers
{
    [Route("api/getToken")]
    [ApiController]
	[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ValuesController : ControllerBase
    {
		private static string getTokenPBI()
		{
			WebClient client = new WebClient();
			client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
			try{
				byte[] response = client.UploadValues("https://login.microsoftonline.com/common/oauth2/token", new NameValueCollection() {
                    { "grant_type", "password" },
                    { "scope", "openid" },
                    { "resource", "https://analysis.windows.net/powerbi/api" },
                    { "client_id", "371fbd15-59c3-4f4b-89b5-e53dcc2e303b" },
                    { "username", "" },
                    { "password", "" },
                    { "client_secret", "0VpLWAeCgTp198xODpaickz1sJLOvZ29ZNQXt+qnPOw=" }
                });

				string result = Encoding.UTF8.GetString(response);

                JObject a = JObject.Parse(result);

                Console.WriteLine(a);

				return "{\"token\":\""+a.GetValue("access_token").ToString()+"\"}";
			}catch(WebException ex){
				return new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
			}
		}

        // GET api/getToken
        [HttpGet]
        public String Get()
        {
			return getTokenPBI();
        }
    }
}
